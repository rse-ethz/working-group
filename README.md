# Working Group

## Website

- Organize regular website updates, news section?
- Improve website: content, structure, accessability (a11y).


## Outreach

- How to organize RSE ambassadors in departments
- Unified presentation slides?
- Get in contact with ETH communications ('ETH news' article?)
- Contact  https://www.vmitet.ethz.ch/ ?
- Approach department IT groups

## Community Survey

- @dominikhaas:matrix.org started to design a survey
- Possible topics:

  - Expectations from community
  - Work situation
  - Research aspects
  - Techincal aspects
  - Personal situation
  - What kind of events? What are good times?

- Also survey for under represented groups (gender, ethnics, mental issues, disabilities)

- Check https://softwaresaved.github.io/international-survey-2022/ for ideas
- There might be other RSE surveys


## RSE Trainings

- check what other countries are doing: https://us-rse.org/wg/education_training/
- Contact Jeremy Cohen (jhc02@doc.ic.ac.uk) (byte sized RSE)
- Contact authors from https://arxiv.org/pdf/2311.11457.pdf (they work on RSE training ideas)
- Sync with events working group to offer trainings / workshops
- Collect, review, structure and publish training material for different levels and research domains.
- Low entry trainings / meetups to attract members.
